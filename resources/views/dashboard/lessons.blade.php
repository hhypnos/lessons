@extends('layouts.master')
@section('content')
    <section class="maindashboard">
    <div class="container ">
        <div class="row">
            @if(count($lessons)>0)
                @foreach($lessons as $lesson)

                    <div class="col-md-4 col-lg-3 b-lable"><a href="{{$lesson->lessonFileURL}}" @if($lesson->lessonFileURL != '#') target="_blank" @endif><button class="btn btn-danger">{{$lesson->lessonName}}</button></a></div>
                @endforeach
                @else
                <div class="col-md-4 col-lg-3 b-lable" style="margin-left:37%"><a href="{{url()->previous()}}"><button class="btn btn-danger">Go Back</button></a></div>
            @endif
        </div>

    </div>
    </section>
@endsection