# Lessons

##Mini Permission system for lessons

![alt screen1](scrn1.png)

![alt screen2](scrn2.png)

![alt screen2](scrn3.png)

## Requirement

- Composer
- PHP 7.2.* +

## Contribution

- clone project
- Open [ProjectFolder] with terminal and write
  - "composer install"
- configure .env file
  - Create database
  - Fill required info for DB in .env file
  - Run Command "php artisan key:generate"
- Open [ProjectFolder] with terminal and write
  - "php artisan migrate"
  - "composer dump-autoload"
