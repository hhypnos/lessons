<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('changelogs',function(){
    return view('changelog')->with(['title'=>'ChangeLogs']);
});

Route::group(['middleware' => ['guest']], function () {
    Route::get('/', 'MainController@login')->name('login');
    Route::post('login', 'MainController@signup')->name('signup');
});


Route::group(['middleware' => ['auth']], function () {
    Route::get('dashboard', 'MainController@dashboard')->name('dashboard');
    Route::get('grade/{gradeid}', 'MainController@grade')->name('grade');
    Route::get('subject/{subjectid}', 'MainController@lessons')->name('grade');

    Route::get('logout', 'MainController@logout')->name('logout');
});