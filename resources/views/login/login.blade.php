@extends('layouts.master')
@section('content')

    <header>
        @include('layouts.errors')
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="login-panel">
                <form action="{{URL::to('login')}}" method="post" >
                    {{csrf_field()}}
                    <img src="{{URL::to('assets/images/logo_white.png')}}" alt="" width="120">
                    <br><br>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary btn-login">Log In</button>
                </form>
            </div>
            <ol class="carousel-indicators">
                @php($i=0)
                @foreach($cover_images as $cover_image)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{$i++}}" class="@if ($loop->first) active @endif"></li>
                @endforeach
            </ol>
            <div class="carousel-inner" role="listbox">
                @foreach($cover_images as $cover_image)
                    <div class="carousel-item @if ($loop->first) active @endif" style="background-image: url({{$cover_image->image}})">
                        <div class="carousel-caption d-none d-md-block">
                        </div>
                    </div>
                    @endforeach


            </div>

            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>
@endsection