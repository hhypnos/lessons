@extends('layouts.master')
@section('content')
    <section class="maindashboard">
    <div class="container ">
        <div class="row">
            @if(count($folders)>0)
        @foreach($folders as $folder)
            <div class="col-md-4 col-lg-3 b-lable"><a href="{{URL::to('grade/'.$folder->id)}}"><button class="btn btn-danger">{{$folder->folderName}}</button></a></div>
        @endforeach
            @else
                <div class="col-md-4 col-lg-3 b-lable" style="margin-left:37%"><a href="#"><button class="btn btn-danger">Nothing is here</button></a></div>
            @endif
        </div>

    </div>
    </section>
@endsection