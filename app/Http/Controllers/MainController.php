<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
class MainController extends Controller
{
    public function login()
    {
        $cover_images = \App\Cover_image::orderBy('order_id','asc')->get();
        return view('login.login',compact('cover_images'))->with(['title'=>'Future Laboratory','description'=>'FutureLab']);
    }

    public function signup()
    {
        if(User::where('email', request('email'))->exists()) {
            $user = User::where('email', request('email'))->first();
            if ($user->userLevel != 0) {
                if (!auth()->attempt(array('email' => request('email'), 'password' => request('password')), false)) {
                    session(['errorType'=>'info']);
                    return back()->withErrors(['info'=>'Incorrect password']);
                }
                session()->forget('errorType');
                return redirect('dashboard');
            } else {
                session(['errorType'=>'error']);
                return back()->withErrors(['info'=>'User Blocked']);
            }
        }
        session(['errorType'=>'warning']);
        return back()->withErrors(['info'=>'User Doesn"t exists']);
    }

    public function dashboard()
    {

        $folders = \App\Folder::orderBy('order_id','asc')
            ->where('folderLevel','<=', auth()->user()->userLevel)
            ->where('folderLevel','>', '0')
            ->orWhere('userID',auth()->user()->id)
            ->get();//get folders by level
        if(auth()->user()->userLevel > 3) {
            $folders = \App\Folder::get();
        }

        return view('dashboard.dashboard',compact('folders'))->with(['title'=>'Future Laboratory','description'=>'FutureLab']);
    }

    public function grade($gradeid)
    {
        $subjects = \App\Subject::orderBy('order_id','asc')
            ->where('folderID',$gradeid)
            ->where('subjectLevel','<=', auth()->user()->userLevel)
            ->where('subjectLevel','>', '0')
            ->orWhere('userID',auth()->user()->id)
            ->get();//get folders by level
        return view('dashboard.grade',compact('subjects'))->with(['title'=>'Future Laboratory','description'=>'FutureLab']);
    }

    public function lessons($subjectid)
    {
        $lessons = \App\Lesson::orderBy('order_id','asc')
            ->where('subjectID',$subjectid)
            ->where('lessonLevel','<=', auth()->user()->userLevel)
            ->where('lessonLevel','>', '0')
            ->orWhere('userID',auth()->user()->id)
            ->get();//get folders by level

        return view('dashboard.lessons',compact('lessons'))->with(['title'=>'Future Laboratory','description'=>'FutureLab']);
    }

    public function logout()
    {

        auth()->logout();

        return redirect('/');
    }
}
