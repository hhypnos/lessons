<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="{{env('APP_URL')}}" />
    @foreach($languages as $language)
        <link rel="alternate" hreflang="{{$language->langAcronym}}" href="{{env('APP_URL')}}/language/{{$language->langID}}" />
    @endforeach
    <meta property="og:site_name" content="{{env('APP_NAME')}}" />
    <meta property="og:title" content="{{$title}}" />
    <meta name="og:description" content="{{$description}}" />
    <meta property="og:url" content="{{env('APP_URL')}}" />
    <meta property="og:image" content="{{URL::to('assets/images/logo.png')}}" />
    <meta property="og:type" content="website" />

    <link rel="apple-touch-icon" sizes="180x180" href="#">
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL::to('assets/images/logo_black.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::to('assets/images/logo_black.png')}}">
    <link rel="shortcut icon" type="image/png" href="{{URL::to('assets/images/logo_black.png')}}">
    <link rel="mask-icon" href="" color="#a3ce54">
    <meta name="apple-mobile-web-app-title" content="{{env('APP_NAME')}}">
    <meta name="application-name" content="{{env('APP_NAME')}}">
    <meta name="theme-color" content="#a3ce54">
    <meta name="description" content="{{$description}}" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="{{$title}}" />
    <script type="application/ld+json">
        {
          "@context": "https:\/\/schema.org",
          "@type": "WebSite",
          "url": "{{url()->full()}}",
          "name": "{{$title}}",
          "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+(995)-557-17-11-44",
            "contactType": "Customer service"
          },
          "logo":"{{URL::to('assets/images/fav.ico')}}",
          "sameAs":["https:\/\/www.facebook.com\/InnovationConsultingFirm"]

        }
</script>
    <title>{{$title}}</title>
    {{--jquery--}}
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    {{--styles--}}

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::to('assets/css/pnotify.custom.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('assets/css/style.css')}}">



</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top">
    <div class="container">
       @if(Request::segment(1) != env('APP_URL')) <a class="navbar-brand" href="#" style="color:white"><img src="{{URL::to('assets/images/logo_white.png')}}" width="48" alt=""></a> @endif
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                @foreach($menus as $menu)
                <li class="nav-item active">
                    <a class="nav-link" href="{{$menu->slug}}" target="_blank">{{$menu->menuName}}
                    </a>
                </li>
                @endforeach

            </ul>
        </div>
    </div>
</nav>