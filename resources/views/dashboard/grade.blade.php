@extends('layouts.master')
@section('content')
    <section class="maindashboard">
    <div class="container ">
        <div class="row" style="text-align: center">
                @foreach($subjects as $subject)
                <div class="col-md-12 b-lable"><a href="{{URL::to('subject/'.$subject->id)}}"><button class="btn btn-danger">{{$subject->subjectName}}</button></a></div>
            @endforeach
                    <div class="col-md-4 col-lg-3 b-lable" style="margin-left:37%"><a href="{{URL::to('dashboard')}}"><button class="btn btn-danger">Go Back</button></a></div>
        </div>

    </div>
    </section>
@endsection